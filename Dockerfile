# https://hub.docker.com/_/docker
FROM docker:27.4.1-git

# https://kubernetes.io/releases/
ARG KUBECTL_VERSION=1.32.0

# https://github.com/helm/helm/releases
ARG HELM_VERSION=3.16.4

ENV HOME=/config

RUN apk add --no-cache curl ca-certificates git sed

RUN wget -O /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl && \
    chmod +x /usr/local/bin/kubectl && \
    kubectl version --client

RUN set -x && \
    wget -O /tmp/helm.tar.gz https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz && \
    tar -xzvf /tmp/helm.tar.gz && \
    mv linux-amd64/helm /usr/local/bin/helm && \
    rm -rf linux-amd64 /tmp/helm.tar.gz && \
    chmod +x /usr/local/bin/helm && \
    helm version

# Create non-root user (with a randomly chosen UID/GUI).
RUN adduser deployer -Du 2342 -h /config

USER deployer
